FROM ubuntu:21.04

ARG TZ=UTC

# Install deps
RUN set -ex \
	&& ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
    && apt update -yqq \
    && apt upgrade -yqq \
    && apt install --no-install-recommends -yqq \
        libpq5 python3 python3-pip \
        gcc git python3-dev libpq-dev \
        yarn \
    && pip3 install --no-cache-dir --upgrade pip \
    # Clean apt
    && rm -rf \
      /var/lib/apt/lists/* \
      /tmp/* \
      /var/tmp/* \
      /usr/share/man \
      /usr/share/doc \
      /usr/share/doc-base
